import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { ReadNewsRoutingModule } from "./readnews-routing.module";
import { ReadNewsComponent } from "./readnews.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ReadNewsRoutingModule
    ],
    declarations: [
        ReadNewsComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ReadNewsModule { }
