let express = require("express"), cors = require("cors");
let app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var noticias = [
	"literatura paris", "futbol barcelona", "futbol barranquilla", "politica monteviedo", "economia santiago de chile", "cocina mexico df", "maria", "brayan", "elvys", "matt", "abc", "bcd", "cde", "def", "panama"
];

app.get("/get", (req, res, next) => 
	res.json(noticias.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

let misFavoritos = [];
app.get("/favs", (req, res, next) => res.json(misFavoritos));
app.post("/favs", (req, res, next) => {
	console.log(req.body);
	misFavoritos.push(req.body.nuevo);
	res.json(misFavoritos);
});